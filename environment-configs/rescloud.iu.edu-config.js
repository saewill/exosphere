"use strict";

var config = {
  showDebugMsgs: false,
  cloudCorsProxyUrl: "https://rescloud.iu.edu/proxy",
  urlPathPrefix: "exosphere",
  palette: {
    light: {
      primary: {
        r: 150,
        g: 35,
        b: 38,
      },
      secondary: {
        r: 0,
        g: 0,
        b: 0,
      },
    },
    dark: {
      primary: {
        r: 255,
        g: 70,
        b: 95,
      },
      secondary: {
        r: 0,
        g: 0,
        b: 0,
      },
    },
  },
  logo: "assets/img/rescloud-logo-white.svg",
  favicon: "assets/img/rescloud-favicon.ico",
  appTitle: "Exosphere for ResCloud",
  topBarShowAppTitle: false,
  defaultLoginView: "oidc",
  aboutAppMarkdown:
    "This is the Exosphere interface for [ResCloud](https://TODO). If you require assistance, please email TODO and specify you are using rescloud.iu.edu.\\\n\\\nUse of this site is subject to the Exosphere hosted sites [Privacy Policy](https://gitlab.com/exosphere/exosphere/-/blob/master/docs/privacy-policy.md) and [Acceptable Use Policy](https://gitlab.com/exosphere/exosphere/-/blob/master/docs/acceptable-use-policy.md).",
  supportInfoMarkdown:
    "See the [documentation](https://TODO) to learn about using Exosphere with ResCloud.\\\n\\\nIn particular, please read about [instance management actions](https://TODO) or [troubleshooting](https://TODO) for answers to common problems before submitting a request to support staff.",
  userSupportEmailAddress: "TODO",
  userSupportEmailSubject:
    "[ResCloud] Support Request From Exosphere for ResCloud",
  openIdConnectLoginConfig: {
    keystoneAuthUrl: "https://rci.uits.iu.edu:5000/identity/v3",
    webssoKeystoneEndpoint:
      "/auth/OS-FEDERATION/websso/openid?origin=https://rescloud.iu.edu/exosphere/oidc-redirector",
    oidcLoginIcon: "assets/img/access-logo.jpg",
    oidcLoginButtonLabel: "Add ACCESS Account",
    oidcLoginButtonDescription: "Recommended login method for ResCloud",
  },
  localization: {
    openstackWithOwnKeystone: "cloud",
    openstackSharingKeystoneWithAnother: "region",
    unitOfTenancy: "allocation",
    maxResourcesPerProject: "quota",
    pkiPublicKeyForSsh: "SSH public key",
    virtualComputer: "instance",
    virtualComputerHardwareConfig: "flavor",
    cloudInitData: "boot script",
    commandDrivenTextInterface: "web shell",
    staticRepresentationOfBlockDeviceContents: "image",
    blockDevice: "volume",
    share: "share",
    accessRule: "share rule",
    exportLocation: "export location",
    nonFloatingIpAddress: "internal IP address",
    floatingIpAddress: "public IP address",
    publiclyRoutableIpAddress: "public IP address",
    graphicalDesktopEnvironment: "web desktop",
    hostname: "hostname",
  },
  instanceConfigMgtRepoUrl: null,
  instanceConfigMgtRepoCheckout: null,
};
